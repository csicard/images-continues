package cs108;

public final class Complex {
    private final double re, im;

    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double squaredModulus() {
        return re * re + im * im;
    }

    public Complex squared() {
        return new Complex(re * re - im * im, 2d * re * im);
    }

    public Complex add(Complex that) {
        return new Complex(re + that.re, im + that.im);
    }
}

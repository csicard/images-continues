package cs108;

import static java.lang.Math.cos;
import static java.lang.Math.floor;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * Une image continue et infinie, représentée par une fonction associant une
 * valeur d'un type donné (p.ex. une couleur) à chaque point du plan.
 */

@FunctionalInterface
public interface Image<T> {
    public T apply(float x, float y);

    public static final Image<ColorRGB> RED_DISK = (x, y) -> {
        double r = sqrt(x * x + y * y);
        return r <= 1d ? ColorRGB.RED : ColorRGB.WHITE;
    };

    public static Image<ColorRGB> chessboard(ColorRGB c1, ColorRGB c2, float w) {
        if (! (w > 0))
            throw new IllegalArgumentException("non-positive width: " + w);

        return (x, y) -> {
            int sqX = (int)floor(x / w), sqY = (int)floor(y / w);
            return (sqX + sqY) % 2 == 0 ? c1 : c2;
        };
    }

    public static <T> Image<T> constant(T v) {
        return (x, y) -> v;
    }

    public static Image<Float> mandelbrot(int maxIt) {
        if (! (maxIt > 0))
            throw new IllegalArgumentException();

        return (x, y) -> {
            Complex c = new Complex(x, y);
            Complex z = c;
            int i = 1;
            while (i < maxIt && z.squaredModulus() <= 4d) {
                z = z.squared().add(c);
                i += 1;
            }
            return (float)i / (float)maxIt;
        };
    }

    public static final Image<Float> HORIZONTAL_GRADIENT_MASK =
            (x, y) -> max(0, min((x + 1f) / 2f, 1f));

    public static Image<ColorRGB> composed(Image<ColorRGB> bg, Image<ColorRGB> fg, Image<Float> mask) {
        return (x, y) -> bg.apply(x, y).mixWith(fg.apply(x, y), mask.apply(x, y));
    }

    public default Image<T> rotated(float angle) {
        float cosA = (float)cos(-angle);
        float sinA = (float)sin(-angle);
        return (x, y) -> {
            float x1 = x * cosA - y * sinA;
            float y1 = x * sinA + y * cosA;
            return apply(x1, y1);
        };
    }
}
